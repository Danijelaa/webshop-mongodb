package rs.laniteo.mongodb.web.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rs.laniteo.mongodb.model.Product;
import rs.laniteo.mongodb.model.User;
import rs.laniteo.mongodb.service.ProductService;
import rs.laniteo.mongodb.support.ProductDTOToProduct;
import rs.laniteo.mongodb.support.ProductToProductDTO;
import rs.laniteo.mongodb.support.Validation;
import rs.laniteo.mongodb.web.dto.ProductDTO;

@RestController
@RequestMapping(value="/products")
public class ProductController {

	
	@Autowired
	private ProductToProductDTO toProductDto;
	@Autowired
	private ProductService ps;
	@Autowired
	private Validation validation;
	@Autowired
	private ProductDTOToProduct toProduct;
	/*@RequestMapping(method=RequestMethod.GET)
	ResponseEntity<List<ProductDTO>> allProducts(@RequestParam Integer pageNumber){
		List<Product> products=ps.getAllProducts(pageNumber);
		return new ResponseEntity<>(toProductDto.convert(products), HttpStatus.OK);
	}*/
	
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	ResponseEntity<ProductDTO> getOne(@PathVariable String id){
		Product product=ps.getById(new ObjectId(id));
		if(product==null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(toProductDto.convert(product), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.GET)
	ResponseEntity<List<ProductDTO>> findByPriceAndCategory(@RequestParam(required=false) Double min, @RequestParam(required=false) Double max, @RequestParam(required=false) String category, @RequestParam(required=false) Integer sortOrder, @RequestParam(required=false) Integer pageNumber){
		List<Product> products=null;
		if(pageNumber==null || pageNumber<=0) {
			pageNumber=1;
		}
		if(category==null) {
			products=ps.getAllProducts(pageNumber);
			return new ResponseEntity<>(toProductDto.convert(products), HttpStatus.OK);
		}
		products=ps.getByPriceAndCategory(min, max, category, sortOrder, pageNumber);
		return new ResponseEntity<>(toProductDto.convert(products), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.PUT, value="/{id}")
	ResponseEntity<?> updateProduct(@RequestBody ProductDTO updatedProduct, @PathVariable String id, HttpSession session){
		User moderator=(User) session.getAttribute("user");
		if(moderator==null || !moderator.getUsername().equals("moderator")){
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		boolean validated=validation.validateUpdatedProduct(updatedProduct);
		if(!validated) {
			HttpHeaders hrs=new HttpHeaders();
			hrs.add("Content-Type", "text/plain");
			return new ResponseEntity<>("Parameters: id, price, quantity and category can not miss.\nQuantity can not be less then zero.\nPrice can not be zero or less.", hrs, HttpStatus.BAD_REQUEST);
		}
		if(!updatedProduct.getId().equals(id)) {
			HttpHeaders hrs=new HttpHeaders();
			hrs.add("Content-Type", "text/plain");
			return new ResponseEntity<>("Ids do not match.", hrs, HttpStatus.BAD_REQUEST);
		}
		Product product=ps.getById(new ObjectId(id));
		if(product==null) {
			HttpHeaders hrs=new HttpHeaders();
			hrs.add("Content-Type", "text/plain");
			return new ResponseEntity<>("Can not update non-existent product.", hrs, HttpStatus.NOT_FOUND);
		}
		product=toProduct.convert(updatedProduct);
		ps.updateProduct(product);
		return new ResponseEntity<>(toProductDto.convert(product), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.POST)
	ResponseEntity<?> addProduct(@RequestBody ProductDTO newProduct, HttpSession session){
		User moderator=(User) session.getAttribute("user");
		if(moderator==null || !moderator.getUsername().equals("moderator")){
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		boolean validated=validation.validateNewProduct(newProduct);
		if(!validated) {
			HttpHeaders hrs=new HttpHeaders();
			hrs.add("Content-Type", "text/plain");
			return new ResponseEntity<>("Parameters: title, price, quantity and category can not miss.\nQuantity can not be less then zero.\nPrice can not be zero or less.", hrs, HttpStatus.BAD_REQUEST);
		}
		Product product=toProduct.convert(newProduct);
		boolean added=ps.addProduct(product);
		if(!added) {
			HttpHeaders hrs=new HttpHeaders();
			hrs.add("Content-Type", "text/plain");
			return new ResponseEntity<>("Sorry. :( \nCould not add product.", hrs, HttpStatus.BAD_REQUEST);
		}
		product=ps.getById(product.getId());
		return new ResponseEntity<>(toProductDto.convert(product), HttpStatus.OK);
	}
	
	@RequestMapping(method=RequestMethod.DELETE, value="/{id}")
	ResponseEntity<?> deleteProduct(@PathVariable String id, HttpSession session){
		User moderator=(User) session.getAttribute("user");
		if(moderator==null || !moderator.getUsername().equals("moderator")){
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		Product product=ps.getById(new ObjectId(id));
		if(product==null) {
			HttpHeaders hrs=new HttpHeaders();
			hrs.add("Content-Type", "text/plain");
			return new ResponseEntity<>("Can not delete non-existent product.", hrs, HttpStatus.NOT_FOUND);
		}
		boolean deleted=ps.deleteProduct(new ObjectId(id));
		if(!deleted) {
			HttpHeaders hrs=new HttpHeaders();
			hrs.add("Content-Type", "text/plain");
			return new ResponseEntity<>("Sorry. Could not delete product.", hrs, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
	
	@RequestMapping(value="/log")
	ResponseEntity<?>logging(){
		boolean error=ps.logTrial();
		if(!error){
			return new ResponseEntity<>(HttpStatus.OK);
		}
		return new ResponseEntity<>(HttpStatus.EXPECTATION_FAILED);
	}
	
	
}
