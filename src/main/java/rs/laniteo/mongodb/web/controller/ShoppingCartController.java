package rs.laniteo.mongodb.web.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import rs.laniteo.mongodb.model.CartItem;
import rs.laniteo.mongodb.model.ShoppingCart;
import rs.laniteo.mongodb.model.User;
import rs.laniteo.mongodb.service.ProductService;
import rs.laniteo.mongodb.service.ShoppingCartService;
import rs.laniteo.mongodb.support.CartItemDTOToCartItem;
import rs.laniteo.mongodb.support.ShoppingCartDTOToShoppingCart;
import rs.laniteo.mongodb.support.ShoppingCartToShoppingCartDTOOut;
import rs.laniteo.mongodb.support.Validation;
import rs.laniteo.mongodb.web.dto.CartItemDTO;
import rs.laniteo.mongodb.web.dto.ShoppingCartDTO;

@RestController
@RequestMapping(value="/shopping-carts")
public class ShoppingCartController {

	@Autowired
	ProductService ps;
	@Autowired
	Validation validation;
	@Autowired
	ShoppingCartService scs;
	@Autowired
	CartItemDTOToCartItem toCartItem;
	@Autowired
	ShoppingCartToShoppingCartDTOOut toShoppingCartDtoOut;
	@Autowired
	ShoppingCartDTOToShoppingCart toShoppingCart;
	
	@RequestMapping(method=RequestMethod.GET)
	ResponseEntity<?> getShoppingCarts(HttpSession session){
		User user=(User) session.getAttribute("user");
		if(user==null){
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		List<ShoppingCart> shoppingCarts=scs.getUsersShoppingCarts(user.getUsername());
		return new ResponseEntity<>(toShoppingCartDtoOut.convert(shoppingCarts), HttpStatus.OK);
	}
	/*
	@RequestMapping(method=RequestMethod.GET, value="/{id}")
	ResponseEntity<?> getShoppingCartById(@RequestParam String id){
		ShoppingCart sc=scs.findById(new ObjectId(id));
		if(sc==null){
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
		return new ResponseEntity<>(toShoppingCartDtoOut.convert(sc), HttpStatus.OK);
	}*/
	
	@RequestMapping(method=RequestMethod.POST)
	ResponseEntity<?> createShoppingCart(@RequestBody List<CartItemDTO> cartItemDtos, HttpSession session){
		User user=(User) session.getAttribute("user");
		if(user==null){
			return new ResponseEntity<>(HttpStatus.FORBIDDEN);
		}
		if(cartItemDtos.size()==0){
			HttpHeaders hrs=new HttpHeaders();
			hrs.add("Content-Type", "text/plain");
			return new ResponseEntity<>("Can not create shopping cart out of none cart ites.", hrs, HttpStatus.BAD_REQUEST);
		}
		boolean validated=validation.validateCartItems(cartItemDtos);
		if(!validated){
			HttpHeaders hrs=new HttpHeaders();
			hrs.add("Content-Type", "text/plain");
			return new ResponseEntity<>("Parameters product_id or quantity missing for one or more cart items.\nQuantity can not be less then zero.", hrs, HttpStatus.BAD_REQUEST);
		}
		cartItemDtos=validation.removeDuplicateItems(cartItemDtos);
		
		ShoppingCart shoppingCart=toShoppingCart.convert(cartItemDtos);
		shoppingCart.setUser_id(user.getUsername());
		ObjectId id=scs.addShoppingCart(shoppingCart);
		if(id==null){
			HttpHeaders hrs=new HttpHeaders();
			hrs.add("Content-Type", "text/plain");
			return new ResponseEntity<>("Sorry. Could not create shopping cart.\nProduct with given id not found or there is not enough products.", hrs, HttpStatus.BAD_REQUEST);
		}
		shoppingCart=scs.findById(id);
		if(shoppingCart==null){
			HttpHeaders hrs=new HttpHeaders();
			hrs.add("Content-Type", "text/plain");
			return new ResponseEntity<>("Sorry. Could not retrieve created shopping cart.", hrs, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<>(toShoppingCartDtoOut.convert(shoppingCart),HttpStatus.OK);
	}
}
