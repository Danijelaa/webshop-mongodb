package rs.laniteo.mongodb.web.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import rs.laniteo.mongodb.model.CartItem;


public class ShoppingCartDTO {

	private List<CartItemDTO> items=new ArrayList<>();
	
	public List<CartItemDTO> getItems() {
		return items;
	}

	

	public void setItems(List<CartItemDTO> items) {
		this.items = items;
	}


	
}
