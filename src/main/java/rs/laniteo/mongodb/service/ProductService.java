package rs.laniteo.mongodb.service;

import java.util.List;

import org.bson.types.ObjectId;

import rs.laniteo.mongodb.model.Product;

public interface ProductService {

	boolean logTrial();
	List<Product> getAllProducts(Integer pageNumber);
	Product getById(ObjectId id);
	List<Product> getByPriceAndCategory(Double min, Double max, String category, Integer sortOrder, Integer pageNumber);
	//List<Product> getByCategory(String category);
	boolean updateProduct(Product product);
	boolean addProduct(Product product);
	boolean deleteProduct(ObjectId id);
	boolean changeQuantityOfProduct(ObjectId product_id, Integer bougthQuantity);
	Integer countProducts();
}
