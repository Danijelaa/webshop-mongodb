package rs.laniteo.mongodb.service;

import java.util.List;

import org.bson.types.ObjectId;

import rs.laniteo.mongodb.model.CartItem;
import rs.laniteo.mongodb.model.ShoppingCart;

public interface ShoppingCartService {

	ObjectId addShoppingCart(ShoppingCart shoppingCart);
	List<ShoppingCart> getUsersShoppingCarts(String user_id);
	ShoppingCart findById(ObjectId id);
}
