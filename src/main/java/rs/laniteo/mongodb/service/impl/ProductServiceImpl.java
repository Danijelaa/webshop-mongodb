package rs.laniteo.mongodb.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.bson.Document;
import org.bson.types.ObjectId;
import org.springframework.stereotype.Service;

import com.mongodb.MongoClient;
import com.mongodb.WriteResult;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.result.UpdateResult;

import rs.laniteo.mongodb.model.Product;
import rs.laniteo.mongodb.service.ProductService;
import rs.laniteo.mongodb.support.CreateConnection;

@Service
public class ProductServiceImpl implements ProductService {

	private static MongoCollection<Document> log=CreateConnection.log();
	@Override
	public List<Product> getAllProducts(Integer pageNumber) {
		List<Product> products=new ArrayList<>();
		Integer start=(pageNumber-1)*4;
		//Integer end=start+4;
		MongoDatabase database=CreateConnection.getConnection();
		//MongoCollection<Product> productCollection=database.getCollection("product", Product.class);
		//MongoCursor<Product> cursor=productCollection.find().iterator();
		MongoCursor<Product> cursor=database.getCollection("product", Product.class).find().limit(4).skip(start).iterator();
		
		try {
			while(cursor.hasNext()) {
				Product product=(Product)cursor.next();
				products.add(product);
				//System.out.println(product.getCategory()+"|"+product.getTitle()+"|"+product.getPrice()+"|"+product.getQuantity()+"|"+product.getId());
			}
		} catch (Exception e) {
			e.printStackTrace();
		}finally {
			cursor.close();
		}
		return products;
	}

	@Override
	public Product getById(ObjectId id) {
		Product product=null;
		MongoDatabase database=CreateConnection.getConnection();
		product=database.getCollection("product", Product.class).find(Filters.eq("_id", id)).first();
		return product;
	}

	@Override
	public boolean updateProduct(Product product) {
		MongoDatabase database=CreateConnection.getConnection();
		MongoCollection<Product> productCollection=database.getCollection("product", Product.class);
		Document findOne=new Document();
		findOne.append("_id", product.getId());
		//Document update=new Document();
		//update.append("$set", new Document().append("price", product.getPrice()));
		long result=productCollection.replaceOne(Filters.eq("_id", product.getId()), product).getModifiedCount();
		return result==1;
	}

	@Override
	public boolean addProduct(Product product) {
		MongoDatabase database=CreateConnection.getConnection();
		MongoCollection<Product> productCollection=database.getCollection("product", Product.class);
		try {
			productCollection.insertOne(product);
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
		return true;
	}

	@Override
	public boolean deleteProduct(ObjectId id) {
		MongoDatabase database=CreateConnection.getConnection();
		MongoCollection<Product> productCollection=database.getCollection("product", Product.class);
		long result=productCollection.deleteOne(Filters.eq("_id", id)).getDeletedCount();
		return result==1;
	}

	@Override
	public Integer countProducts() {
		MongoDatabase database=CreateConnection.getConnection();
		MongoCollection<Product> productCollection=database.getCollection("product", Product.class);
		//Integer 
		return null;
	}

	@Override
	public List<Product> getByPriceAndCategory(Double min, Double max, String category, Integer sortOrder,Integer pageNumber) {
		List<Product> products=new ArrayList<>();
		MongoDatabase database=CreateConnection.getConnection();
		Document query=new Document("category", category);
		if(min==null) {
			min=0.0;
		}
		Document queryPrice=new Document("$gte", min);
		if(max!=null) {
			queryPrice.append("$lte", max);
		}
		query.append("price", queryPrice);
		if(sortOrder==null || !(sortOrder==1 || sortOrder==-1)) {
			sortOrder=1;
		}
		Integer start=(pageNumber-1)*4;
		MongoCursor<Product> cursor=database.getCollection("product", Product.class).find(query).sort(new Document("price", sortOrder)).limit(4).skip(start).iterator();
		
		while(cursor.hasNext()){
			Product product=cursor.next();
			//System.out.println(product.getCategory()+"|"+product.getTitle()+"|"+product.getPrice()+"|"+product.getQuantity()+"|"+product.getId());
			products.add(product);
		}
		return products;
	}

	@Override
	public boolean changeQuantityOfProduct(ObjectId product_id, Integer bougthQuantity) {
		MongoDatabase database=CreateConnection.getConnection();
		MongoCollection<Product> productCollection=database.getCollection("product", Product.class);
		Product product=getById(product_id);
		if(product.getQuantity()<bougthQuantity) {
			return false;
		}
		long result=productCollection.updateOne(Filters.eq("_id", product.getId()), new Document().append("$inc", new Document("quantity", bougthQuantity*(-1)))).getModifiedCount();
		return result==1;
	}

	@Override
	public boolean logTrial() {
		try {
			MongoCollection<Product> productCollection=CreateConnection.getConnection().getCollection("product", Product.class);
			productCollection.updateOne(Filters.eq("x"), new Document());
		} catch (Exception e) {
			Document doc=new Document();
			doc.append("date", new Date());
			doc.append("message", e.getMessage());
			log.insertOne(doc);
			e.printStackTrace();
			return false;
		}
		return true;
	}

}
