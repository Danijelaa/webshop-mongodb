package rs.laniteo.mongodb.service;

import org.bson.types.ObjectId;

import rs.laniteo.mongodb.model.User;

public interface UserService {

	User findByUserNameAndPassword(String username, String password);
}
