package rs.laniteo.mongodb.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import rs.laniteo.mongodb.model.CartItem;
import rs.laniteo.mongodb.model.ShoppingCart;
import rs.laniteo.mongodb.service.ProductService;
import rs.laniteo.mongodb.web.dto.CartItemDTO;
import rs.laniteo.mongodb.web.dto.ShoppingCartDTO;

@Component
public class ShoppingCartDTOToShoppingCart implements Converter<List<CartItemDTO>, ShoppingCart>{

	@Autowired
	ProductService ps;
	@Autowired
	CartItemDTOToCartItem toCartItem;
	
	@Override
	public ShoppingCart convert(List<CartItemDTO> itemsDto) {
		 ShoppingCart shoppingCart=new ShoppingCart();
		 List<CartItem> items=toCartItem.convert(itemsDto);
		 shoppingCart.setItems(items);
		return shoppingCart;
	}

}
