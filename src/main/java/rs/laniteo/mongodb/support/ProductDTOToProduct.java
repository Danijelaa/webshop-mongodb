package rs.laniteo.mongodb.support;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import rs.laniteo.mongodb.model.Product;
import rs.laniteo.mongodb.service.ProductService;
import rs.laniteo.mongodb.web.dto.ProductDTO;

@Component
public class ProductDTOToProduct implements Converter<ProductDTO, Product> {

	@Autowired
	private ProductService ps;
	
	@Override
	public Product convert(ProductDTO productDto) {
		Product product;
		if(productDto.getId()==null) {
			product=new Product();
			product.setId(new ObjectId());
			product.setTitle(productDto.getTitle());
		}
		else {
			product=ps.getById(new ObjectId(productDto.getId()));
		}
		product.setCategory(productDto.getCategory());
		product.setPrice(productDto.getPrice());
		product.setQuantity(productDto.getQuantity());
		return product;
	}

}
