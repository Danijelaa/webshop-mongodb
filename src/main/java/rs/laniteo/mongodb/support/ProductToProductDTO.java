package rs.laniteo.mongodb.support;

import java.util.ArrayList;
import java.util.List;

import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import rs.laniteo.mongodb.model.Product;
import rs.laniteo.mongodb.web.dto.ProductDTO;

@Component
public class ProductToProductDTO implements Converter<Product, ProductDTO>{

	@Override
	public ProductDTO convert(Product product) {
		ProductDTO productDto=new ProductDTO();
		productDto.setCategory(product.getCategory());
		productDto.setId(product.getId().toString());
		productDto.setPrice(product.getPrice());
		productDto.setQuantity(product.getQuantity());
		productDto.setTitle(product.getTitle());
		return productDto;
	}

	public List<ProductDTO> convert(List<Product> products){
		List<ProductDTO> productDtos=new ArrayList<>();
		for(Product p:products) {
			productDtos.add(convert(p));
		}
		return productDtos;
	}
}
